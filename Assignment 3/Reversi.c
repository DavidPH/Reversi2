#include "Reversi.h"

#define NO_OF_PLAYERS 2
extern int No_Disks;
void boardInitialise(int Board[BOARD_LENGTH][BOARD_LENGTH])
{
    Board[3][3] = 1;
    Board[4][4] = 1;
    Board[4][3] = -1;
    Board[3][4] = -1;
}
struct PlayerStatus initialisePlayer(struct PlayerStatus player[])
{
    char *type[2] = {"Black","White"};
    int i;
    for(i = 0;i < NO_OF_PLAYERS;i++)
    {
        printf("\nPlease enter the name for player %d:\n",i+1);
        if(i == 0)
            puts("Note: Player 1 will be black and will start the game:");
        //Gets the user name from the user.
        fgets(player[i].playerName,MAX_LENGTH,stdin);
        //Gets rid of the \n for neater printing
        player[i].playerName[strcspn( player[i].playerName,"\n")] = 0;
        //Starts at 0 disks as a separate function will count the number on the board.
        player[i].disksOnBoard = 0;
        //Assigns the player the colour; Player 1 is black, Player 2 is white.
        strcpy(player[i].colour,type[i]);
    }
    playerC = BLACK;//This sets it so the first move will be black.

}

struct PlayerStatus colourCount(int Board[BOARD_LENGTH][BOARD_LENGTH],struct PlayerStatus player[])
{
    int i,j;
    //Reset the number as it needs to be recounted. A move add or remove multiple disks.
    player[0].disksOnBoard = 0;
    player[1].disksOnBoard = 0;
    for(i = 0;i < 8;i++)
    {
        for(j = 0;j < 8;j++)
        {
            if(Board[i][j] == Black)
            {
                player[0].disksOnBoard++;
            }
            else if(Board[i][j] == White)
            {

                player[1].disksOnBoard++;
            }
        }
    }
}

void displayStatus(struct PlayerStatus player[],int DiskstoPlay)
{
    int i;
    //Prints the status of the player.
    for(i = 0;i < NO_OF_PLAYERS;i++)
    {
        printf("\n\nPlayer %d: %s \nColour: %s",i+1,player[i].playerName,player[i].colour);
    }
    printf("\n\nScore\n%s %d:%d %s",player[0].colour,player[0].disksOnBoard,player[1].disksOnBoard,player[1].colour);
    printf("\nDisks available to play: %d\n",DiskstoPlay);

    //Adds the win condition.
    if(player[0].disksOnBoard == 0)
    {
        printf("\nPlayer %d: %s is the winner.",2,player[1].playerName);
        gameStatus = OVER;
    }
    else if(player[1].disksOnBoard == 0)
    {
        printf("\nPlayer %d: %s is the winner\nGame Over",1,player[0].playerName);
        gameStatus = OVER;
    }
    else
    {
        if(DiskstoPlay == 0)
        {
            if(player[0].colour > player[1].colour)
            {
                printf("\nPlayer %d: %s is the winner\nGame Over",1,player[0].playerName);
                gameStatus = OVER;
            }
            else if(player[1].colour > player[0].colour)
            {
                printf("\nPlayer %d: %s is the winner\nGame Over",2,player[1].playerName);
                gameStatus = OVER;
            }
            else
            {
                puts("Game is a tie.");
                gameStatus = OVER;
            }
        }
    }
}

void printBoard(int Board[BOARD_LENGTH][BOARD_LENGTH]){

    //Prints the Board
    // Board[0][0] means x is 1 and y is 1.
    // If Board[i][j] == 1 then it prints an 'O' if == -1 it prints an 'X'. 0 means no piece.


    int i,j;
    int Xcount,Ycount; // X and Y coordinates
    printf("\n");
    for(j=0;j<8;j++){
        printf("  %d  ",j+1);
    }
    printf("\n");
    for(j=0;j<8;j++) {
        printf("|---|"); //Prints the first line of the board.
    }
    printf("\n");
    //Every loop iteration prints 1 square only
    for(i =1,Xcount=1,Ycount=1;i<=64;i++,Xcount++){

        if (Xcount == 9) {  //Keeps track on which square and column the loop is in
            Ycount++;
            Xcount = 1;
            for(j=0;j<8;j++) {
                printf("|---|");
            }
            printf("\n");
        }

        //Prints a piece if there's one on the square being checked.
        if (checkForPiece(Board,Xcount,Ycount)) {
            if(Board[Xcount-1][Ycount-1] == -1){
                printf("| %c |",'B');
            }
            else if(Board[Xcount-1][Ycount-1] == 1){
                printf("| %c |",'W');
            }
            else if(Board[Xcount-1][Ycount-1] == 2){
                printf("| %c |",'+');
            }
            if(Xcount==8){
                printf(" %d",Ycount);
            }
        }
        //
        else {
            printf("|   |");
            if(Xcount==8){
                printf(" %d",Ycount);
            }
        }
        if (i % 8 == 0) {  //Every 8 squares it moves down a line
            printf("\n");
        }
    }
    for(j=0;j<8;j++) {
        printf("|---|"); //Prints the last line of the board
    }
    printf("\n");

}

bool checkForPiece(int Board[BOARD_LENGTH][BOARD_LENGTH],int Xcount, int Ycount){
//Used only in the printBoard function
//Checks if a piece should be printed.
    int i;
    int j;
    bool out=false;

    for(i=0; i< 8; i++) {
        for(j=0; j< 8; j++) {

            if ((Board[i][j] != 0 && i+1 == Xcount && j+1 == Ycount)){
                out = true;
            }

        }
    }
    return out;

}

void placePiece(int Board[BOARD_LENGTH][BOARD_LENGTH], int x, int y, int color){

    x -= 1;
    y -= 1;
    int i,j,w,z,c,f;
    Board[x][y] = color;
//horizontal
    for(z=x+1;z<=8;z++){
        if(Board[z][y]==0){
            break;
        }
        if(Board[z][y]==color){
            for(i=0,w=x+1;i<z;i++,w++){
                if(Board[w][y] == 0){
                    break;
                }
                if(Board[w][y] == color){
                    break;
                }
                if(Board[w][y] != color){
                    Board[w][y] *= -1;
                }
            }
            break;
        }
    }
    for(z=x-1,w=z;z<=8 && w>=0;z++,w--){
        if(Board[w][y]==0){
            break;
        }
        if(Board[w][y]==color){
            for(i=0,w=x-1;i<z && w>=0;i++,w--){
                if(Board[w][y] == 0){
                    break;
                }
                if(Board[w][y] == color){
                    break;
                }
                if(Board[w][y] != color){
                    Board[w][y] *= -1;
                }
            }
            break;
        }
    }
//vertical
    for(z=y+1; z<=8;z++){
        if(Board[x][z]==0){
            break;
        }
        if(Board[x][z]==color){
            for(i=0,w=y+1;i<z;i++,w++){
                if(Board[x][w] == 0){
                    break;
                }
                if(Board[x][w] == color){
                    break;
                }
                if(Board[x][w]!=color){
                    Board[x][w] *=-1;
                }
            }
            break;
        }
    }
    for(i=0,z=y-1;i<=8 && z >=0;i++,z--){
        if(Board[x][z]==0){
            break;
        }
        if(Board[x][z]==color){
            for(i=0,w=y-1;i<=z && w>=0;i++,w--){
                if(Board[x][w] == 0){
                    break;
                }
                if(Board[x][w]==color){
                    break;
                }
                if(Board[x][w]!=color){
                    Board[x][w]*=-1;
                }
            }
            break;
        }
    }
//Diagonal
    for(i=x+1,z=y-1; i<= 8 && z>=0; i++,z--) {  //up toward right
        if(Board[i][z] == 0){
            break;
        }
        if(Board[i][z] == color){
            for(j=x,w=y; j<=i;j++,w--){
                if(Board[j][w] == 0){
                    break;
                }
                if(Board[j][w]!= color){
                    Board[j][w] *= -1;
                }
            }
            break;
        }
    }
    for(i=0 ,z=y-1,c=x-1; i<= 8&& c >=0 && z >=0; i++,z--,c--) { //up toward left
        if(Board[c][z] == 0){
            break;
        }
        if(Board[c][z] == color){
            for(f=x-1,w=y-1; w >= 0 && f >= 0;w--,f--){
                if(Board[f][w] == 0){
                    break;
                }
                if(Board[f][w]==color){
                    break;
                }

                if(Board[f][w]!= color){
                    Board[f][w] *= -1;
                }

            }
            break;
        }
    }
    for(i=x+1,z=y+1; i<= 8 && z<=8; i++,z++) { //down toward right
        if(Board[i][z] == 0){
            break;
        }
        if(Board[i][z] == color){
            for(j=x,w=y; j<=i;j++,w++){
                if(Board[j][w]!= color){
                    Board[j][w] *= -1;
                }
            }
            break;
        }
    }
    for(i=x+1,z=y+1,c=x-1; i<= 18 && c>=0 && z<=8; i++,z++,c--) { //down toward left
        if(Board[c][z] == 0){
            break;
        }
        if(Board[c][z] == color){
            for(j=x,w=y,f=x; j<=i;j++,w++,f--){
                if(Board[f][w]!= color){
                    Board[f][w] *= -1;
                }
            }
            break;
        }
    }
}


bool moveValidator(int Board[BOARD_LENGTH][BOARD_LENGTH])
{
    int i, j, a, d;
    unsigned int b = 0, c = 0;
    int coOrdinates[32][2] = {0};
    bool valid, aMove = false,isMove = false;

    //Black == -1
    if (playerC == BLACK)
    {
        for (i = 0; i < 8; i++)
        {
            for (j = 0; j < 8; j++)
            {
                if (Board[i][j] == 0)//Checks for a valid move if the current piece is empty.
                {
                    valid = false;

                    if (Board[i + 1][j] == White)
                    {
                        //Checking if the vertical line works,then check horizontal then diagonals.
                        for (a = i + 2; a < 8 && !valid; a++)//Checks down vertically
                        {
                            if (Board[a][j] == Black)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    //These are ifs and not else ifs as the program may need to go  to every single statement.
                    if (Board[i - 1][j] == White && !valid)//This and the following if statements only trigger if the previous statement did not return a valid move.
                    {
                        for (a = i - 2; a >= 0 && !valid; a--)//Checks vertically up.
                        {
                            if (Board[a][j] == Black)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i][j + 1] == White && !valid)//Checks horizontally right.
                    {
                        for (a = j + 2; a < 8 && !valid; a++)
                        {
                            if (Board[i][a] == Black)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i][j - 1] == White && !valid)//Checks horizontally left.
                    {
                        for (a = j - 2; a >= 0 && !valid; a--)
                        {
                            if (Board[i][a] == Black)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i - 1][j - 1] == White && !valid)//Checks diagonally up left
                    {
                        for (a = i - 2, d = j - 2; (a >= 0 && d >= 0) && !valid; a--, d--)
                        {
                            if (Board[a][d] == Black)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i - 1][j + 1] == White && !valid)//Checks diagonally up right.
                    {
                        for (a = i - 2, d = j + 2; a >= 0 && d < 8 && !valid; a--, d++)
                        {
                            if (Board[a][d] == Black)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i + 1][j - 1] == White && !valid)//Checks diagonally down left.
                    {
                        for (a = i + 2, d = j - 2; a < 8 && d >= 0 && !valid; a++, d--)
                        {
                            if (Board[a][d] == Black)
                            {
                                //Assigns the location to the co-ordinate arrays.
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;//Increments the values for future use.
                                valid = true;
                            }
                        }
                    }
                    //This loop is overwriting co-ordinate arrays.
                    if (Board[i + 1][j + 1] == White && !valid)//Checks diagonally down right.
                    {
                        for (a = i + 2, d = j + 2; a < 8 && d < 8 && !valid; a++, d++)
                        {
                            if (Board[a][d] == Black)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (valid)
                    {
                        aMove = true;
                        isMove = true;
                    }
                }
            }
        }
        //Checks if there is at least 1 valid move. If there is one, then it will move on with making a move etc.
        //Else it will say there were no valid moves and let the other player move.
        if(aMove)
        {
            validMoves(coOrdinates, b,Board);
            makeMove(coOrdinates, b,Board);
            playerC = WHITE;
        }
        else
        {
            puts("Black has no valid moves.");
            playerC = WHITE;
        }
    }
    else if (playerC == WHITE)
    {
        for (i = 0; i < 8; i++)
        {
            for (j = 0; j < 8; j++)
            {
                if (Board[i][j] == 0)//Checks for a valid move if the current piece is empty.
                {
                    valid = false;

                    if (Board[i + 1][j] == Black)
                    {
                        //Checking if the vertical line works,then check horizontal then diagonals.
                        for (a = i + 2; a < 8 && !valid; a++)//Checks down vertically
                        {
                            if (Board[a][j] == White)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    //These are ifs and not else ifs as the program may need to go  to every single statement.
                    if (Board[i - 1][j] == Black && !valid)//This and the following if statements only trigger if the previous statement did not return a valid move.
                    {
                        for (a = i - 2; a >= 0 && !valid; a--)//Checks vertically up.
                        {
                            if (Board[a][j] == White)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i][j + 1] == Black && !valid)//Checks horizontally right.
                    {
                        for (a = j + 2; a < 8 && !valid; a++)
                        {
                            if (Board[i][a] == White)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i][j - 1] == Black && !valid)//Checks horizontally left.
                    {
                        for (a = j - 2; a >= 0 && !valid; a--)
                        {
                            if (Board[i][a] == White)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i - 1][j - 1] == Black && !valid)//Checks diagonally up left
                    {
                        for (a = i - 2, d = j - 2; (a >= 0 && d >= 0) && !valid; a--, d--)
                        {
                            //puts("hello");
                            if (Board[a][d] == White)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i - 1][j + 1] == Black && !valid)//Checks diagonally up right.
                    {
                        for (a = i - 2, d = j + 2; a >= 0 && d < 8 && !valid; a--, d++)
                        {
                            if (Board[a][d] == White)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (Board[i + 1][j - 1] == Black && !valid)//Checks diagonally down left.
                    {
                        for (a = i + 2, d = j - 2; a < 8 && d >= 0 && !valid; a++, d--)
                        {
                            if (Board[a][d] == White)
                            {
                                //Assigns the location to the co-ordinate arrays.
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;//Increments the values for future use.
                                valid = true;
                            }
                        }
                    }
                    //This loop is overwriting co-ordinate arrays.
                    if (Board[i + 1][j + 1] == Black && !valid)//Checks diagonally down right.
                    {
                        for (a = i + 2, d = j + 2; a < 8 && d < 8 && !valid; a++, d++)
                        {
                            if (Board[a][d] == White)
                            {
                                coOrdinates[b][0] = i;
                                coOrdinates[b][1] = j;
                                b++;
                                c++;
                                valid = true;
                            }
                        }
                    }
                    if (valid)
                    {
                        aMove = true;
                        isMove = true;
                    }
                }
            }
        }
        //Checks if there is at least 1 valid move. If there is one, then it will move on with making a move etc.
        //Else it will say there were no valid moves and let the other player move.
        if(aMove)
        {
            validMoves(coOrdinates, b,Board);
            makeMove(coOrdinates, b,Board);
            playerC = BLACK;
        }
        else
        {
            puts("White has no valid moves.");
            playerC = BLACK;
        }
    }

    return isMove;
}

void validMoves(int coOrdinates[32][2],unsigned int count,int Board[BOARD_LENGTH][BOARD_LENGTH])
{
    int i,j,k;
    int moveBoard[BOARD_LENGTH][BOARD_LENGTH];

    //Creates a mimic board to show valid moves on the board itself.
    for(i = 0;i < BOARD_LENGTH;i++)
    {
        for(j = 0;j < BOARD_LENGTH;j++)
            moveBoard[i][j] = Board[i][j];
    }
    //Adds the co-ordinates to the mimic board.
    for(i = 0;i < count;i++)
    {
        j = coOrdinates[i][0];
        k = coOrdinates[i][1];

        moveBoard[j][k] = 2;
    }
    printBoard(moveBoard);

    puts("+ Indicates valid move on the board.");
    if(playerC == BLACK){
        puts("Black's turn!");
    }
    else{
        puts("White's turn!");
    }
    puts("List of available moves\n________________________________________\n");

    //Prints out the list of moves.
    for(i = 0;i < (count);i++){
        if(i== count-1){//For formatting so no comma on last move.
            printf("(%d,%d)",coOrdinates[i][0]+1,coOrdinates[i][1]+1);
        }
        else{
            printf("(%d,%d),",coOrdinates[i][0]+1,coOrdinates[i][1]+1);
        }

    }

    printf("\n________________________________________\n\n");
}

void makeMove(int coOrdinates[32][2],unsigned int count,int Board[BOARD_LENGTH][BOARD_LENGTH])
{
    int x,y;
    unsigned int i,j;
    int color;
    bool valid = false;
    if(playerC == BLACK){
        color = -1;
    }
    else{
        color =1;
    }
    for(i = 0;i < count;i++)//I stored the values of the array, not the printed number location. Had to increment all values by 1.
    {
        for(j = 0;j < 2;j++)
            coOrdinates[i][j]++;
    }

    int c = 0;
    while(!valid)//Checks if the move is valid. If not then it will continue asking for a valid move.
    {
        puts("Please enter a valid x and y co-ordinate");
        scanf("%d%d",&x,&y);

        for(i = 0;i < count;i++)
        {
            if((x == coOrdinates[i][0] && x != 0) && (y == coOrdinates[i][1] && y != 0))
            {
                valid = true;
            }
        }
        //Just for a bit of fun (c = amount of times looped)
        c++;
        if(c==4){
            puts("Are you okay?");
        }
        if(c==7){
            puts("You're worrying me now.");
        }
        if(c==11){
            puts("You do see the list of valid moves right?");
        }
        if(c==15){
            puts("Are you having a stroke?\nWould you like us to call the emergency services?");
        }
        //
    }

    placePiece(Board,x,y,color);
    No_Disks--;
}

