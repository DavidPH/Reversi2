#include "Reversi.h"
#define NO_OF_PLAYERS 2
#define MAX_LENGTH 25
int No_Disks = 60;

int main(void)
{
    //Array containing the board status
    //Value of -1 for black and 1 for white. A value of 0 means no piece.
    int Board[BOARD_LENGTH][BOARD_LENGTH] = { 0 }; //Array is initialized with all values of 0;
    bool wasMove;
    gameStatus = CONTINUE;

    //Initialises the player struct
    struct PlayerStatus Players[NO_OF_PLAYERS];
    //Calls the functions
    boardInitialise(Board);
    initialisePlayer(Players);
    colourCount(Board,Players);
    displayStatus(Players,No_Disks);
   // printBoard(Board);
    while(gameStatus == CONTINUE)
    {
        wasMove = moveValidator(Board);
        if(wasMove == 1)
        {
            colourCount(Board, Players);
            displayStatus(Players, No_Disks);
            //printBoard(Board);
        }

    }
    printBoard(Board);

    return 0;
}

