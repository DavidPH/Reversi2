#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#define MAX_LENGTH 25
#define BOARD_LENGTH 8
#define White 1
#define Black (-1)

typedef struct PlayerStatus
{
    char playerName[MAX_LENGTH];
    int disksOnBoard;
    char colour[6];
};

enum Status {OVER,CONTINUE};
enum Status gameStatus;

enum colour {BLACK,WHITE};
enum colour playerC;

void boardInitialise(int Board[BOARD_LENGTH][BOARD_LENGTH]);

struct PlayerStatus initialisePlayer(struct PlayerStatus player[]);

struct PlayerStatus colourCount(int Board[BOARD_LENGTH][BOARD_LENGTH],struct PlayerStatus player[]);

void displayStatus(struct PlayerStatus player[],int DiskstoPlay);

void printBoard(int Board[BOARD_LENGTH][BOARD_LENGTH]);

bool checkForPiece(int Board[BOARD_LENGTH][BOARD_LENGTH], int Xcount, int Ycount);

bool moveValidator(int Board[BOARD_LENGTH][BOARD_LENGTH]);

void validMoves(int coOrdinates[32][2],unsigned int count,int Board[BOARD_LENGTH][BOARD_LENGTH]);

void makeMove(int coOrdinates[32][2],unsigned int count,int Board[8][8]);
